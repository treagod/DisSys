package ah.marv;

import org.springframework.stereotype.Component;

@Component
public class Product {
	private int id;
	private String name;
	private String description;
	private double price;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return name;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public void receiveMessage(String message) {
		System.out.println(message);
	}

	@Override
    public String toString() {
        return "Product{" +
                "name=" + name +
                ", description='" + description + '\'' +
                ", price='" + price + '\'' +
                '}';
    }
}

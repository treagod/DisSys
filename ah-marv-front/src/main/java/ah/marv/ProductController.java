package ah.marv;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class ProductController {
		
	private final ProductService productService;
	private final OrderService orderService;
	
	public ProductController(ProductService productService, OrderService orderService) {
		this.productService = productService;
		this.orderService = orderService;
	}
	@GetMapping(path="/products")
	public String getAllProducts(Model model) {
		Iterable<Product> prod = productService.getProducts();
		model.addAttribute("products", prod);
		return "products";
	}
	
	@GetMapping(path="/products/{productId}")
	public String newProduct(@PathVariable String productId, Model model) {
		int id = Integer.parseInt(productId);
		Product product = productService.getProduct(id);
		model.addAttribute("product", product);
		return "productId";
	}
	
	@GetMapping(path="/products/new")
	public String newProduct(Model model) {
		model.addAttribute("product", new Product());
		return "newProduct";
	}
	
	@PostMapping(path="/products")
	public String createProduct(@ModelAttribute Product product) {
		productService.addProduct(product);
		return "redirect:products";
	}
}

package ah.marv;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
@RefreshScope
public class ProductService {
	
	@Value("${queue-name.products:ah-marv-products}")
	private String queName;
	
	@Value("${services.product.all://marv-products/products}")
	private String serviceName;
	
	@Autowired
	RestTemplate rest;
	
	@Autowired
	RabbitTemplate rabbitTemplate;
	
	public void addProduct(Product product) {
		rabbitTemplate.convertAndSend(queName, product.getName()+";"+product.getDescription()+";"+product.getPrice());
	}

	@SuppressWarnings("unused")
	public Iterable<Product> getFallbackProducts() {
		return new ArrayList<Product>();
	}
	
	@HystrixCommand(fallbackMethod = "getFallbackProducts")
	public Iterable<Product> getProducts() {
		Product[] products = rest.getForObject(serviceName, Product[].class);
		return Arrays.asList(products);
	}

	public Product getProduct(int id) {
		Product product = rest.getForObject(serviceWithId(id), Product.class);
		return product;
	}
	
	private String serviceWithId(int id) {
		return serviceName + "/" + id;
	}
}

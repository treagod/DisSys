package ah.marv;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class OrderController {
		
	private final OrderService orderService;
	private final ProductService productService;
	
	public OrderController(OrderService orderService, ProductService productService) {
		this.orderService = orderService;
		this.productService = productService;
	}
	
	@PostMapping(path="/orders")
	public String createOrderForProduct(@ModelAttribute Order order) {
		orderService.addOrder(order);
		return "redirect:products/" + order.getProductId();
	}
	
	@GetMapping(path="/orders/new")
	public String newProduct(Model model) {
		model.addAttribute("order", new Order());
		model.addAttribute("products", productService.getProducts());
		return "newOrder";
	}
}

package ah.marv;

import java.util.Date;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RefreshScope
public class OrderService {
	
	@Value("${queue-name.orders:ah-marv-orders}")
	private String queName;
	
	@Value("${services.order.all://marv-products/orders}")
	private String serviceName;
	
	@Autowired
	RestTemplate rest;
	
	@Autowired
	RabbitTemplate rabbitTemplate;
	
	public void addOrder(Order order) {
		rabbitTemplate.convertAndSend(queName, "" + order.getProductId() + ";" + new Date().toString());
	}

	public int getCountForProduct(int id) {
		Count count = rest.getForObject("//marv-orders/products/"+id+"/orders/count", Count.class);
		return count.getCount();
	}
}

package ah.marv.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ah.marv.model.Product;

public interface ProductsRepository extends JpaRepository<Product,Long> {
}

package ah.marv.repositories;

import org.springframework.stereotype.Component;

import ah.marv.model.Product;

@Component
public class RabbitListenerProducts {
	ProductsRepository pr;
	
	public RabbitListenerProducts(ProductsRepository pr) {
		this.pr = pr;
	}
	
	public void setProductRepository(ProductsRepository pr) {
		this.pr = pr;
	}
	
	public void receiveMessage(String product) {
		String[] parts = product.split(";");
		Product prod = new Product();
		prod.setName(parts[0]);
		prod.setDescription(parts[1]);
		prod.setPrice(Double.parseDouble(parts[2]));
		pr.save(prod);
	}
}

package ah.marv.controllers;

import java.util.List;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ah.marv.model.Product;
import ah.marv.repositories.ProductsRepository;
import ah.marv.repositories.RabbitListenerProducts;

@RestController
public class ProductsRestController {
	
	@Value("${queue-name.products:ah-marv-products}")
	private String queName;
	
	@Autowired
	RabbitTemplate rabbitTemplate;
	
	@Bean
	Queue queue() {
		return new Queue(queName, false);
	}
	
	@Bean TopicExchange exchange() {
		return new TopicExchange("ah-marv-exchange");
	}
	
	@Bean
	Binding binding(Queue queue, TopicExchange exchange) {
		return BindingBuilder.bind(queue).to(exchange).with(queName);
	}
	
	@Bean
	SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
			MessageListenerAdapter listenreAdapter) {
		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
		container.setConnectionFactory(connectionFactory);
		container.setQueueNames(queName);
		container.setMessageListener(listenreAdapter);
		
		return container;
	}
	
	@Bean
	MessageListenerAdapter listenerAdapter(RabbitListenerProducts product) {
		product.setProductRepository(productsRepository);
		return new MessageListenerAdapter(product, "receiveMessage");
	}

    private final ProductsRepository productsRepository;


    public ProductsRestController(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }
    
    @RequestMapping(path = "/products", method = RequestMethod.POST)
    public Product create(@RequestBody Product product) {
    	productsRepository.save(product);
        return product;
    }

    @GetMapping(path = "/products")
    public List<Product> getAllProducts() {
        return productsRepository.findAll();
    }
    
    @GetMapping(path = "/products/{productId}")
    public Product getProduct(@PathVariable String productId) {
    	long id = Long.parseLong(productId);
    	return productsRepository.findOne(id);
    }
}

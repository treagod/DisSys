package ah.marv.controllers;

import java.util.List;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ah.marv.model.Count;
import ah.marv.model.Orders;
import ah.marv.repositories.OrdersRepository;
import ah.marv.repositories.RabbitListenerOrders;

@RestController
public class OrdersRestController {
	@Value("${queue-name.orders:ah-marv-orders}")
	private String queName;
	
	@Autowired
	RabbitTemplate rabbitTemplate;
	
	@Bean
	Queue queue() {
		return new Queue(queName, false);
	}
	
	@Bean TopicExchange exchange() {
		return new TopicExchange("ah-marv-exchange");
	}
	
	@Bean
	Binding binding(Queue queue, TopicExchange exchange) {
		return BindingBuilder.bind(queue).to(exchange).with(queName);
	}
	
	@Bean
	SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
			MessageListenerAdapter listenreAdapter) {
		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
		container.setConnectionFactory(connectionFactory);
		container.setQueueNames(queName);
		container.setMessageListener(listenreAdapter);
		
		return container;
	}
	
	@Bean
	MessageListenerAdapter listenerAdapter(RabbitListenerOrders orderListener) {
		orderListener.setProductRepository(orderRepository);
		return new MessageListenerAdapter(orderListener, "receiveMessage");
	}

    private final OrdersRepository orderRepository;


    public OrdersRestController(OrdersRepository orderRepository) {
        this.orderRepository = orderRepository;
    }
    
    @RequestMapping(path = "/orders", method = RequestMethod.POST)
    public Orders create(@RequestBody Orders order) {
    	
    	orderRepository.save(order);
        return order;
    }

    @GetMapping(path = "/orders")
    public List<Orders> getAllProducts() {
        return orderRepository.findAll();
    }
    
    @GetMapping(path = "/products/{productId}/orders/count")
    public Count getOrderCount(@PathVariable String productId) {
    	Long prodId = Long.parseLong(productId);
    	
    	List<Orders> orders = orderRepository.findAll();
    	int count = 0;
    	for (Orders order : orders) {
    		if (order.getProductId() == prodId) {
    			count++;
    		}
    	}
    	Count coun = new Count();
    	coun.setCount(count);
    	return coun;
    }
}

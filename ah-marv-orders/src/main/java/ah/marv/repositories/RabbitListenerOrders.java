package ah.marv.repositories;

import java.util.Date;

import org.springframework.stereotype.Component;

import ah.marv.model.Orders;

@Component
public class RabbitListenerOrders {
	OrdersRepository or;
	
	public void setProductRepository(OrdersRepository or) {
		this.or = or;
	}
	
	public void receiveMessage(String product) {
		String[] parts = product.split(";");
		int productId = Integer.parseInt(parts[0]);
		Orders order= new Orders();
		order.setProductId(productId);
		order.setOrderedAt(new Date());
		or.save(order);
	}
}

package ah.marv.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ah.marv.model.Orders;

public interface OrdersRepository extends JpaRepository<Orders,Long> {
}

package ah.marv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class AhMarvEurekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AhMarvEurekaServerApplication.class, args);
	}
}
